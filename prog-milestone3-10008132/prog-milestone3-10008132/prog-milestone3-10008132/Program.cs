﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone3_10008132
{
    class Program
    {
        static void Main(string[] args)
        {
            Customer customer = new Customer();
            UserInput(customer);

            PizzaMenu();

            List<Pizza> pizzaOrder = new List<Pizza>();
            AnotherOrder(pizzaOrder);

            string drink = "";
            double drinkPrice = 0;

            DrinksMenu(ref drink, ref drinkPrice);

            TotalOrder(pizzaOrder, customer, drink, drinkPrice);

            BillPayment(pizzaOrder, drinkPrice);

            ThanksMessage();
        }
/****************************************************************************************************************************************/

        static Customer UserInput(Customer customer)
        {
            Console.WriteLine("***********************************");
            Console.WriteLine("**     Welcome To Hell Pizza     **");
            Console.WriteLine("***********************************");
            Console.WriteLine("");

            Console.Write("Please type your name: ");
            customer.Name = Console.ReadLine();
            Console.WriteLine();
            Console.Write("Type your phone number: ");
            int.TryParse(Console.ReadLine(), out customer.Phnumber);

            return customer;
        }
/****************************************************************************************************************************************/

        static void PizzaMenu()
        {
            Console.Clear();

            Console.WriteLine("************************************");
            Console.WriteLine("\tPlease choose a pizza");
            Console.WriteLine();
            Console.WriteLine("\tMorder");
            Console.WriteLine("\tLust Deluxe");
            Console.WriteLine("\tMischief");
            Console.WriteLine("\tGrimm");
            Console.WriteLine("\tTrouble");
            Console.WriteLine();
            Console.WriteLine("*************************************");
        }
/****************************************************************************************************************************************/

        static Pizza SizeSelection()
        {
            Pizza pizza = new Pizza();

            pizza.Name = Console.ReadLine();

            Console.WriteLine();
            Console.WriteLine("****************************************");
            Console.WriteLine("\tChoose the size of pizza");
            Console.WriteLine();
            Console.WriteLine("\tSmall");
            Console.WriteLine("\tMedium");
            Console.WriteLine("\tLarge");
            Console.WriteLine("*****************************************");

            pizza.Size = Console.ReadLine();

            if (pizza.Size == "small" || pizza.Size == "Small")
            {
                pizza.Price = 5.00;
            }
            else if (pizza.Size == "medium" || pizza.Size == "Medium")
            {
                pizza.Price = 7.00;
            }
            else
            {
                pizza.Price = 10.00;
            }
            return pizza;
        }
/****************************************************************************************************************************************/

        static string DrinksMenu(ref string drink, ref double drinkPrice)
        {
            Console.Clear();

            drinkPrice = 3.00;

            Console.WriteLine("*******************************************");
            Console.WriteLine("\tChoose A Drink");
            Console.WriteLine();
            Console.WriteLine("\tDrinks\tPrice");
            Console.WriteLine();
            Console.WriteLine($"\tFanta\t${drinkPrice}");
            Console.WriteLine($"\tCoke\t${drinkPrice}");
            Console.WriteLine($"\tSprite\t${drinkPrice}");
            Console.WriteLine($"\tLimca\t${drinkPrice}");
            Console.WriteLine($"\tPepsi\t${drinkPrice}");
            Console.WriteLine($"\t7Up\t${drinkPrice}");
            Console.WriteLine("\tNone");
            Console.WriteLine();
            Console.WriteLine("********************************************");

            drink = Console.ReadLine();

            if (drink == "none" || drink == "None")
            {
                drinkPrice = 0;
            }

            return $"{drink}{drinkPrice}";
        }
/****************************************************************************************************************************************/

        static void AnotherOrder(List<Pizza> pizzaOrder)
        {
            Pizza a = SizeSelection();
            pizzaOrder.Add(a);

            Console.WriteLine();
            Console.Write("Do you want to make another order (press <y> or <n>): ");
            char ans = char.Parse(Console.ReadLine());

            if (ans == 'y')
            {
                do
                {
                    Console.Clear();
                    PizzaMenu();

                    Pizza b = SizeSelection();
                    pizzaOrder.Add(b);

                    Console.WriteLine();
                    Console.Write("Do you want to make another order (press <y> or <n>): ");
                    ans = char.Parse(Console.ReadLine());
                } while (ans == 'y');
            }
        }
/****************************************************************************************************************************************/

        static void TotalOrder(List<Pizza> pizzaOrder, Customer customer, string drink, double drinkPrice)
        {
            Console.Clear();

            Console.WriteLine($"You are {customer.Name}");
            Console.WriteLine();

            Console.WriteLine($"Your phone number is {customer.Phnumber}");
            Console.WriteLine();

            foreach (var x in pizzaOrder)
            {
                Console.WriteLine($"You order a {x.Size} {x.Name} pizza of price ${x.Price}");
            }

            Console.WriteLine($"You order a drink {drink} of price ${drinkPrice}");
        }
/****************************************************************************************************************************************/

        static void BillPayment(List<Pizza> pizzaOrder, double drinkPrice)
        {
            Console.Clear();

            double TotalPizzaPrice = 0;
            double TotalDrinkPrice = 0;
            double totalPayment = 0;

            int customerPayment = 0;
            double Change = 0;

// Calculation for total payment
            foreach (var x in pizzaOrder)
            {
                TotalPizzaPrice += x.Price;
            }

            TotalDrinkPrice = drinkPrice;

            totalPayment = TotalPizzaPrice + TotalDrinkPrice;

            Console.WriteLine($"Your bill : ${totalPayment}");
            Console.WriteLine();

// Calculation for change
            Console.WriteLine("Type amount of money you are going to pay");
            int.TryParse(Console.ReadLine(), out customerPayment);

            Change = customerPayment - totalPayment;

            Console.WriteLine($"Your change : ${Change}");
            Console.WriteLine();
            Console.WriteLine("----------Press <ENTER>----------");
            Console.ReadLine();
        }
/****************************************************************************************************************************************/
        static void ThanksMessage()
        {
            Console.Clear();

            Console.WriteLine("*************************************");
            Console.WriteLine("**       Thanks For Shopping       **");
            Console.WriteLine("**                                 **");
            Console.WriteLine("**        Please Come Again        **");
            Console.WriteLine("**                                 **");
            Console.WriteLine("*************************************");
        }
     }
/****************************************************************************************************************************************/

    public class Customer
    {
        public string Name;
        public int Phnumber;
    }
    public class Pizza
    {
        public string Name;
        public string Size;
        public double Price;
    }
    // Program End
}
